﻿using BillManagerAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BillManagerAPI.Contexts
{
    public class CountriesContext : DbContext
    {
       public CountriesContext(DbContextOptions<CountriesContext> options)
            : base(options) { }


        public DbSet<Countries> Countries { get; set; }

    }
}
