﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BillManagerAPI.Models;

namespace BillManagerAPI.DataProvider
{
    public interface IBillManagerDataProvider
    {
        Task SaveCMARequest(BillManager billmanager);
        Task SaveCMRequest(BillManager billmanager);
        Task SaveSALRequest(BillManager billmanager);
        Task BilledTotalRemove(BillManager billmanager);
        Task BilledTotalAdd(BillManager billmanager);
    }

    public interface IBilledTotalDataProvider
    {
        Task<IEnumerable<BilledTotal>> GetBilledTotal(int trialID);
    }

    public interface ICreditRequestsDataProvider
    {
        Task<IEnumerable<CreditRequests>> GetCreditRequests(string ClientID, string ProtocolID = "",
            int InvestigatorID = 0, string SiteName = "", string Patients = "",
            string Visits = "");

        Task<IEnumerable<CreditRequests>> GetSALRequests(string ClientID, string ProtocolID = "",
            int InvestigatorID = 0, string SiteName = "", string Patients = "",
            string Visits = "");
    }

    public interface IBillRequestsDataProvider
    {
        Task SaveSALRequest(BillRequests billrequests);
    }

}