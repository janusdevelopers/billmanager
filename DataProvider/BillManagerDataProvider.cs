﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BillManagerAPI.Models;

namespace BillManagerAPI.DataProvider
{
    public class BillManagerDataProvider : IBillManagerDataProvider
    {
        private readonly string connectionString = "Server=PAYDEV2;Database=CFSAP;Trusted_Connection=True;";

        private SqlConnection sqlConnection;

        public async Task SaveCMARequest(BillManager billmanager)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@RequestID", billmanager.RequestID);
                dynamicParameters.Add("@RequestType", billmanager.RequestType);
                dynamicParameters.Add("@PTVID", billmanager.PTVID);
                dynamicParameters.Add("@PTVPID", billmanager.PTVPID);
                dynamicParameters.Add("@patientID", billmanager.patientID);
                dynamicParameters.Add("@visitID", billmanager.VisitID);
                dynamicParameters.Add("@VisitAmount", billmanager.Visitamount);
                dynamicParameters.Add("@VisitDate", billmanager.VisitDate);
                dynamicParameters.Add("@RequestStatus", billmanager.RequestStatus);
                dynamicParameters.Add("@Requester", billmanager.Requester);
                dynamicParameters.Add("@Approver", billmanager.Approver);
                dynamicParameters.Add("@SALStatusCode", billmanager.SALStatusCode);
                dynamicParameters.Add("@RequestComments", billmanager.Requestcomments);
                dynamicParameters.Add("@SponsorID", billmanager.SponsorID);
                dynamicParameters.Add("@ProtocolID", billmanager.ProtocolID);
                dynamicParameters.Add("@TrialID", billmanager.TrialID);
                dynamicParameters.Add("@investigatoriD", billmanager.InvestigatorID);
                dynamicParameters.Add("@SAPInvNo", billmanager.SAPInvNo);
                dynamicParameters.Add("@RqCreatedDate", billmanager.Rqcreateddate);
                dynamicParameters.Add("@RqApproved", billmanager.RqApproved);

                await sqlConnection.ExecuteAsync(
                    "dbo.csp_Insite_BillRequest_Save",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task SaveCMRequest(BillManager billmanager)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@RequestID", billmanager.RequestID);
                dynamicParameters.Add("@RequestType", billmanager.RequestType);
                dynamicParameters.Add("@PTVID", billmanager.PTVID);
                dynamicParameters.Add("@PTVPID", billmanager.PTVPID);
                dynamicParameters.Add("@patientID", billmanager.patientID);
                dynamicParameters.Add("@visitID", billmanager.VisitID);
                dynamicParameters.Add("@VisitAmount", billmanager.Visitamount);
                dynamicParameters.Add("@VisitDate", billmanager.VisitDate);
                dynamicParameters.Add("@RequestStatus", billmanager.RequestStatus);
                dynamicParameters.Add("@Requester", billmanager.Requester);
                dynamicParameters.Add("@Approver", billmanager.Approver);
                dynamicParameters.Add("@SALStatusCode", billmanager.SALStatusCode);
                dynamicParameters.Add("@RequestComments", billmanager.Requestcomments);
                dynamicParameters.Add("@SponsorID", billmanager.SponsorID);
                dynamicParameters.Add("@ProtocolID", billmanager.ProtocolID);
                dynamicParameters.Add("@TrialID", billmanager.TrialID);
                dynamicParameters.Add("@investigatoriD", billmanager.InvestigatorID);
                dynamicParameters.Add("@SAPInvNo", billmanager.SAPInvNo);
                dynamicParameters.Add("@RqCreatedDate", billmanager.Rqcreateddate);
                dynamicParameters.Add("@RqApproved", billmanager.RqApproved);

                await sqlConnection.ExecuteAsync(
                    "dbo.csp_Insite_BillRequest_Save",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task SaveSALRequest(BillManager billmanager)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@RequestID", billmanager.RequestID);
                dynamicParameters.Add("@RequestType", billmanager.RequestType);
                dynamicParameters.Add("@PTVID", billmanager.PTVID);
                dynamicParameters.Add("@PTVPID", billmanager.PTVPID);
                dynamicParameters.Add("@patientID", billmanager.patientID);
                dynamicParameters.Add("@visitID", billmanager.VisitID);
                dynamicParameters.Add("@VisitAmount", billmanager.Visitamount);
                dynamicParameters.Add("@VisitDate", billmanager.VisitDate);
                dynamicParameters.Add("@RequestStatus", billmanager.RequestStatus);
                dynamicParameters.Add("@Requester", billmanager.Requester);
                dynamicParameters.Add("@Approver", billmanager.Approver);
                dynamicParameters.Add("@SALStatusCode", billmanager.SALStatusCode);
                dynamicParameters.Add("@RequestComments", billmanager.Requestcomments);
                dynamicParameters.Add("@SponsorID", billmanager.SponsorID);
                dynamicParameters.Add("@ProtocolID", billmanager.ProtocolID);
                dynamicParameters.Add("@TrialID", billmanager.TrialID);
                dynamicParameters.Add("@investigatoriD", billmanager.InvestigatorID);
                dynamicParameters.Add("@SAPInvNo", billmanager.SAPInvNo);
                dynamicParameters.Add("@RqCreatedDate", billmanager.Rqcreateddate);
                dynamicParameters.Add("@RqApproved", billmanager.RqApproved);

                await sqlConnection.ExecuteAsync(
                    "dbo.csp_Insite_BillRequest_Save",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task BilledTotalRemove(BillManager billmanager)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@RefNo", billmanager.RequestID);
                dynamicParameters.Add("@InvType", billmanager.RequestType);
                await sqlConnection.ExecuteAsync(
                    "dbo.csp_Insite_BilledTotal_Remove",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task BilledTotalAdd(BillManager billmanager)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@RefNo", billmanager.RequestID);
                dynamicParameters.Add("@InvType", billmanager.RequestType);
                await sqlConnection.ExecuteAsync(
                    "dbo.csp_Insite_BilledTotal_Add",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

    }

    public class BilledTotalDataProvider : IBilledTotalDataProvider
    {
        private readonly string connectionString = "Server=PAYDEV2;Database=CFSAP;Trusted_Connection=True;";

        private SqlConnection sqlConnection;

        public async Task<IEnumerable<BilledTotal>> GetBilledTotal(int trialID)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@trialID", trialID);
                dynamicParameters.Add("@ViewType", 2);

                return await sqlConnection.QueryAsync<BilledTotal>(
                    "dbo.csp_patientVisitsPayStatus_ByApps",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

    }

    public class CreditRequestsDataProvider : ICreditRequestsDataProvider
    {
        private readonly string connectionString = "Server=PAYDEV2;Database=CFSAP;Trusted_Connection=True;";

        private SqlConnection sqlConnection;

        public async Task<IEnumerable<CreditRequests>> GetCreditRequests(string ClientID, string ProtocolID = "",
            int InvestigatorID = 0, string SiteName = "", string Patients = "",
            string Visits = "")
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@prm_ClientID", ClientID);
                dynamicParameters.Add("@prm_ProtocolID", ProtocolID);
                dynamicParameters.Add("@prm_InvestigatorID", InvestigatorID);
                dynamicParameters.Add("@prm_trialID", SiteName);
                dynamicParameters.Add("@prm_PatientsID", Patients);
                dynamicParameters.Add("@prm_VisitID", Visits);

                return await sqlConnection.QueryAsync<CreditRequests>(
                    "dbo.csp_IPMS_CreditRequests",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<CreditRequests>> GetSALRequests(string ClientID, string ProtocolID = "",
            int InvestigatorID = 0, string SiteName = "", string Patients = "",
            string Visits = "")
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@prm_ClientID", ClientID);
                dynamicParameters.Add("@prm_ProtocolID", ProtocolID);
                dynamicParameters.Add("@prm_InvestigatorID", InvestigatorID);
                dynamicParameters.Add("@prm_trialID", SiteName);
                dynamicParameters.Add("@prm_PatientsID", Patients);
                dynamicParameters.Add("@prm_VisitID", Visits);

                return await sqlConnection.QueryAsync<CreditRequests>(
                    "dbo.csp_IPMS_UpdateSALRequests",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }



    }

    public class BillRequestsDataProvider : IBillRequestsDataProvider
    {
        private readonly string connectionString = "Server=PAYDEV2;Database=CFSAP;Trusted_Connection=True;";

        private SqlConnection sqlConnection;

        public async Task SaveBillRequest(BillRequests billrequests)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@RefNo", billrequests.RequestID);
                dynamicParameters.Add("@InvType", billrequests.RequestType);

                dynamicParameters.Add("@RequestID", billrequests.RequestID);
                dynamicParameters.Add("@RequestType", billrequests.RequestType);
                dynamicParameters.Add("@PTVID", billrequests.PTVID);
                dynamicParameters.Add("@PTVPID", billrequests.PTVPID);
                dynamicParameters.Add("@patientID", billrequests.patientID);
                dynamicParameters.Add("@visitID", billrequests.VisitID);
                dynamicParameters.Add("@VisitAmount", billrequests.Visitamount);
                dynamicParameters.Add("@VisitDate", billrequests.VisitDate);
                dynamicParameters.Add("@RequestStatus", billrequests.RequestStatus);
                dynamicParameters.Add("@Requester", billrequests.Requester);
                dynamicParameters.Add("@Approver", billrequests.Approver);
                dynamicParameters.Add("@SALStatusCode", billrequests.SALStatusCode);
                dynamicParameters.Add("@RequestComments", billrequests.Requestcomments);
                dynamicParameters.Add("@SponsorID", billrequests.SponsorID);
                dynamicParameters.Add("@ProtocolID", billrequests.ProtocolID);
                dynamicParameters.Add("@TrialID", billrequests.TrialID);
                dynamicParameters.Add("@investigatoriD", billrequests.InvestigatorID);
                dynamicParameters.Add("@SAPInvNo", billrequests.SAPInvNo);
                dynamicParameters.Add("@RqCreatedDate", billrequests.Rqcreateddate);
                dynamicParameters.Add("@RqApproved", billrequests.RqApproved);

                await sqlConnection.ExecuteAsync(
                    "dbo.csp_Insite_BillRequest_Save",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public Task SaveSALRequest(BillRequests billrequests)
        {
            throw new System.NotImplementedException();
        }
    }
}
