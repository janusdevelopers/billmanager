﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillManagerAPI.Models
{
    public class Countries
    {
       
        [Key]
        public string CountryCode { get; set; }
        public string CountryNumber { get; set; }
        public string CountryName { get; set; }
        public bool IsVisible { get; set; }

      
}
}
