﻿using System;

namespace BillManagerAPI.Models
{
    public partial class BillManager
    {
        public int  RequestID  { get; set; }
        public string  RequestType { get; set; }
        public int  PTVID  { get; set; }
        public int  PTVPID  { get; set; }
        public string patientID { get; set; }
        public int  VisitID  { get; set; }
        public decimal  Visitamount { get; set; }
        public DateTime  VisitDate  { get; set; }
        public string RequestStatus { get; set; }
        public string Requester { get; set; }
        public int  Approver  { get; set; }
        public string SALStatusCode { get; set; }
        public string Requestcomments { get; set; }
        public int  SponsorID  { get; set; }
        public int  ProtocolID  { get; set; }
        public int  TrialID  { get; set; }
        public int  InvestigatorID  { get; set; }
        public int  SAPInvNo  { get; set; }
        public DateTime Rqcreateddate  { get; set; }
        public DateTime RqApproved  { get; set; }
        public int  ApproveType  { get; set; }
    }

    public partial class BilledTotal
    {
        public int TrialID { get; set; }
        public int ViewType { get; set; }
        public int DocNum { get; set; }
        public string DocType { get; set; }
        public decimal SADocTotal { get; set; }
        public decimal IPMSDocTotal { get; set; }
        public string ErrMsg { get; set; }

    }

    public partial class CreditRequests
    {
        public string TrialID { get; set; }
        public string Sitename { get; set; }
		public int Invno { get; set; }
        public int PatientTrialID { get; set; }
		public int PatientID { get; set; }
        public string patientNo { get; set; }
		public string PatientRandom { get; set; }
		public string patientStatus { get; set; }
		public string PatientHold { get; set; }
		public decimal VisitTotal { get; set; }
		public string DocNum { get; set; }
		public string Appname { get; set; }
		public int VisitID { get; set; }
        public string VisitName { get; set; }
		public DateTime VisitDate { get; set; }
        public decimal VisitAmount{ get; set; }
		public int Billed { get; set; }
        public int processed { get; set; }
		public int Inprocess { get; set; }
        public string docType { get; set; }
		public int Receiptno { get; set; }
        public string docStat { get; set; }
		public DateTime createddate{ get; set; }
        public int PTVID { get; set; }
		public int PTVPID { get; set; }
        public string SALColor { get; set; }
    }

    public partial class BillRequests
    {
        public int RequestID { get; set; }
        public string RequestType { get; set; }
        public int PTVID { get; set; }
        public int PTVPID { get; set; }
        public string patientID { get; set; }
        public int VisitID { get; set; }
        public decimal Visitamount { get; set; }
        public DateTime VisitDate { get; set; }
        public string RequestStatus { get; set; }
        public string Requester  { get; set; }
        public int Approver { get; set; }
        public string SALStatusCode { get; set; }
        public string Requestcomments { get; set; }
        public int SponsorID { get; set; }
        public int ProtocolID { get; set; }
        public int TrialID { get; set; }
        public int InvestigatorID { get; set; }
        public int SAPInvNo { get; set; }
        public DateTime Rqcreateddate { get; set; }
        public DateTime RqApproved { get; set; }
        public int ApproveType { get; set; }
    }

}
