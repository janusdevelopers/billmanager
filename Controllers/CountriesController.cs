﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BillManagerAPI.Contexts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BillManagerAPI.Controllers
{
    [Route("api/countries")]
    public class CountriesController : Controller
    {
        private readonly CountriesContext _context;

        public CountriesController(CountriesContext context)
        {
            _context = context;
        }

        [Route("list")]
        [HttpGet]
        public JsonResult Get()
        {
            ///////////////////////////////////////////////////
            //using entity framework to pull the DbSet
            // 
            //////////////////////////////////////////////////
            return Json(_context.Countries);
            
        }

        [Route("country/{code}")]
        [HttpGet]
        public IActionResult Get(string code)
        {
            ///////////////////////////////////////////////////
            //using entity framework to pull the DbSet
            // WHERE Claue filters specific record
            //////////////////////////////////////////////////
            var country = _context.Countries.Where(c => c.CountryCode == code).Single();

            return Json(country);

        }

        [Route("update/{code}/{show}")]
        [HttpGet]
        public IActionResult Get(string code, int show)
        {
            var result = "Updated";
            try
            {
                ///////////////////////////////////////////////////
                //using entity framework to run stored procedures
                // Countries_Show is a TEST sp that is in CFSAP
                //////////////////////////////////////////////////
                _context.Database.ExecuteSqlCommand("Countries_Show @p0, @p1", parameters: new[] { $"{code}", $"{show}" });
                
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
          
            return Ok(result);

        }
    }
}