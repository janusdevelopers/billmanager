﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using BillManagerAPI.DataProvider;
using BillManagerAPI.Models;

namespace BillManagerAPI.Controllers
{
    [Route("api/billmanager")]
    public class BillManagerController : Controller
    {
        private IBillManagerDataProvider billmanagerDataProvider;

        public BillManagerController(IBillManagerDataProvider billmanagerDataProvider)
        {
            this.billmanagerDataProvider = billmanagerDataProvider;
        }

        [HttpPost]
        public async Task SaveCMARequest([FromBody]BillManager billmanager)
        {
            await this.billmanagerDataProvider.SaveCMARequest(billmanager);
        }

        [HttpPost]
        public async Task SaveCMRequest([FromBody]BillManager billmanager)
        {
            await this.billmanagerDataProvider.SaveCMRequest(billmanager);
        }

        [HttpPost]
        public async Task SaveSALRequest([FromBody]BillManager billmanager)
        {
            await this.billmanagerDataProvider.SaveSALRequest(billmanager);
        }

        [HttpPost]
        public async Task BilledTotalRemove([FromBody]BillManager billmanager)
        {
            await this.billmanagerDataProvider.BilledTotalRemove(billmanager);
        }

        [HttpPost]
        public async Task BilledTotalAdd([FromBody]BillManager billmanager)
        {
            await this.billmanagerDataProvider.BilledTotalAdd(billmanager);
        }
    }

    [Route("api/billedtotal")]
    public class BilledTotalController : Controller
    {
        private IBilledTotalDataProvider billedtotalDataProvider;

        public BilledTotalController(IBilledTotalDataProvider billedtotalDataProvider)
        {

            this.billedtotalDataProvider = billedtotalDataProvider;
        }

        [HttpGet("{trialID}")]
        public async Task<IEnumerable<BilledTotal>> GetBilledTotal(int trialID)
        {
            return await this.billedtotalDataProvider.GetBilledTotal(trialID);
        }
    }

    public class CreditRequestsController : Controller
    {
        private ICreditRequestsDataProvider creditrequestsDataProvider;

        public CreditRequestsController(ICreditRequestsDataProvider creditrequestsDataProvider)
        {
            this.creditrequestsDataProvider = creditrequestsDataProvider;
        }

        [HttpGet]
        [Route("creditrequests")]
        public async Task<IEnumerable<CreditRequests>> GetCreditRequests(string ClientID, string ProtocolID = "",
            int InvestigatorID = 0, string SiteName = "", string Patients = "",
            string Visits = "")
        {
            return await this.creditrequestsDataProvider.GetCreditRequests(ClientID, ProtocolID,InvestigatorID, SiteName, Patients,
            Visits);
        }

        [HttpGet]
        [Route("salrequests")]
        public async Task<IEnumerable<CreditRequests>> GetSALRequests(string ClientID, string ProtocolID = "",
            int InvestigatorID = 0, string SiteName = "", string Patients = "",
            string Visits = "")

        {
            return await this.creditrequestsDataProvider.GetSALRequests(ClientID, ProtocolID, InvestigatorID, SiteName, Patients,
            Visits); 
        }

    }
}